<?php
require __DIR__ . '/../vendor/autoload.php';

use Michelf\Markdown;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class Controller
{
    private $recipesDirectory = __DIR__ . '/recipes/';
    private $lockPath = __DIR__ . '/../var/lock';
    private $twig;
    private $env;
    private $request;
    private $session;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->env = ($this->request->getHost() === 'localhost') ? 'dev' : 'prod';
        $twigOptions = [];
        if ($this->env === 'prod') {
            $twigOptions['cache'] = __DIR__ . '/../var/cache';
        } else {
            $twigOptions['debug'] = true;
        }

        $this->session = new Session();

        $loader = new FilesystemLoader(__DIR__ . '/../templates');
        $this->twig = new Environment($loader, $twigOptions);

        if ($this->env === 'dev') {
            $this->twig->addExtension(new Twig\Extension\DebugExtension());
        }
    }

    private function convertSlugToString($slug)
    {
        return ucfirst(str_replace('-', ' ', $slug));
    }

    private function convertStringToSlug($string)
    {
        return trim(preg_replace(
            '/[^a-z0-9\-]/',
            '-',
            strtolower($string)
        ), '-');
    }

    private function extractTags($content) {
        preg_match_all('/@[a-z]*/', $content, $matches);
        return $matches[0];
    }

    private function extractServes($content) {
        preg_match('/serves\s?([0-9]+\s?\-\s?[0-9]+|[0-9]+)/i', $content, $matches);
        return $matches[1];
    }

    private function findRecipes($searchTerm = null)
    {
        $dir = new DirectoryIterator($this->recipesDirectory);
        $files = [];

        foreach ($dir as $file) {
            if (!$file->isDot() && $file->getExtension() === 'md') {
                if ($searchTerm !== null) {
                    $content = file_get_contents($file->getPathname());
                    if (strpos(strtolower($content), strtolower($searchTerm)) !== false) {
                        $files[] = $file->getBasename();
                    }
                } else {
                    $files[] = $file->getBasename();
                }
            }
        }

        sort($files);

        return $files;
    }

    private function renderHomePage()
    {
        $recipeFiles = $this->findRecipes();

        $recipes = [];

        // turn into paths and nice names
        foreach ($recipeFiles as $recipeFile) {
            $recipePath = preg_replace('/\\.md$/', '', $recipeFile);
            $content = file_get_contents($this->recipesDirectory . $recipePath . '.md');
            $recipes[$recipePath] = [
                'name' => $this->convertSlugToString($recipePath),
                'tags' => $this->extractTags($content),
                'serves' => $this->extractServes($content)
            ];
        }

        Response::create($this->twig->render(
            'list.html.twig',
            [
                'pageTitle' => 'Recipes',
                'recipes' => $recipes,
                'flashes' => $this->session->getFlashBag()->all(),
                'locked' => $this->getIsLocked()
            ]
        ))->send();
    }

    private function searchRecipes($searchTerm) {
        $recipeFiles = $this->findRecipes($searchTerm);

        $recipes = [];

        // turn into paths and nice names
        foreach ($recipeFiles as $recipeFile) {
            $recipePath = preg_replace('/\\.md$/', '', $recipeFile);
            $recipes[] = $recipePath;
        }

        Response::create(json_encode($recipes))->send();
    }

    private function renderShowRecipe($recipeSlug)
    {
        $recipeMarkdown = file_get_contents($this->recipesDirectory . $recipeSlug . '.md');

        if ($recipeMarkdown !== false) {
            $recipe = Markdown::defaultTransform($recipeMarkdown);
            Response::create($this->twig->render(
                'show.html.twig',
                [
                    'pageTitle' => $this->convertSlugToString($recipeSlug),
                    'recipeSlug' => $recipeSlug,
                    'recipe' => $recipe,
                    'bodyClass' => 'show',
                    'locked' => $this->getIsLocked()
                ]
            ))->send();
        } else {
            Response::create('Page not found', Response::HTTP_NOT_FOUND)->send();
        }
    }

    private function newRecipe()
    {
        if ($this->getIsLocked() === true) {
            RedirectResponse::create('Forbidden', Response::HTTP_FORBIDDEN)->send();
        }

        if ($this->request->getMethod() === Request::METHOD_POST) {
            $errors = [];
            $recipeName = $this->request->request->get('name');
            $recipeMarkdown = $this->request->request->get('markdown');
            $recipeFile = $this->convertStringToSlug($recipeName);
            $recipePath = $this->recipesDirectory . $recipeFile . '.md';

            if (file_exists($recipePath)) {
                $errors['name'] = 'This name already exists. Try something else.';
            }

            if ($this->request->request->get('name') === '') {
                $errors['name'] = 'Please supply a name.';
            }

            if (count($errors) > 0) {
                $this->renderEditPage('', $recipeName, $recipeMarkdown, $errors);
            } else {
                $saveResult = file_put_contents($recipePath, $recipeMarkdown);

                if ($saveResult !== false) {
                    $this->session->getFlashBag()->add('alert', 'Successfully saved');
                    RedirectResponse::create('/')->send();
                } else {
                    $errors['name'] = 'There was a problem saving the file. Try a different name.';
                    $this->renderEditPage('', $recipeName, $recipeMarkdown, $errors);
                }
            }
        } else {
            Response::create($this->twig->render(
                'new.html.twig',
                [
                    'pageTitle' => 'Create new recipe'
                ]
            ))->send();
        }
    }

    private function editRecipe($recipeSlug)
    {
        if ($this->getIsLocked() === true) {
            RedirectResponse::create('Forbidden', Response::HTTP_FORBIDDEN)->send();
        }

        if ($this->request->getMethod() === Request::METHOD_POST) {
            $errors = [];
            $newRecipeName = $this->request->request->get('name');
            $recipeMarkdown = $this->request->request->get('markdown');
            $oldRecipeFile = $this->convertStringToSlug($recipeSlug);
            $oldRecipePath = $this->recipesDirectory . $oldRecipeFile . '.md';
            $newRecipeFile = $this->convertStringToSlug($newRecipeName);
            $newRecipePath = $this->recipesDirectory . $newRecipeFile . '.md';
            $nameHasChanged = $oldRecipeFile !== $newRecipeFile;

            if ($nameHasChanged && file_exists($newRecipePath)) {
                $errors['name'] = 'This name already exists. Try something else.';
            }

            if ($this->request->request->get('name') === '') {
                $errors['name'] = 'Please supply a name.';
            }

            if (count($errors) > 0) {
                $this->renderEditPage($recipeSlug, $newRecipeName, $recipeMarkdown, $errors);
            } else {
                $saveResult = file_put_contents($newRecipePath, $recipeMarkdown);

                if ($saveResult !== false) {
                    if ($nameHasChanged && file_exists($oldRecipePath)) {
                        unlink($oldRecipePath);
                    }
                    $this->session->getFlashBag()->add('alert', 'Successfully saved');
                    RedirectResponse::create('/show/' . $newRecipeFile)->send();
                } else {
                    $errors['name'] = 'There was a problem saving the file. Try a different name.';
                    $this->renderEditPage($recipeSlug, $newRecipeName, $recipeMarkdown, $errors);
                }
            }
        } else {
            $recipeMarkdown = file_get_contents($this->recipesDirectory . $recipeSlug . '.md');
            if ($recipeMarkdown !== false) {
                $this->renderEditPage($recipeSlug, $this->convertSlugToString($recipeSlug), $recipeMarkdown);
            } else {
                Response::create('Page not found', Response::HTTP_NOT_FOUND)->send();
            }
        }
    }

    private function renderEditPage($recipeSlug, $recipeName, $recipeMarkdown, $errors = [])
    {
        if ($this->getIsLocked() === true) {
            RedirectResponse::create('Forbidden', Response::HTTP_FORBIDDEN)->send();
        }

        Response::create($this->twig->render(
            'edit.html.twig',
            [
                'pageTitle' => 'Edit recipe',
                'recipeSlug' => $recipeSlug,
                'recipeName' => $recipeName,
                'recipeMarkdown' => $recipeMarkdown,
                'errors' => $errors
            ]
        ))->send();
    }

    private function deleteRecipe($recipeSlug)
    {
        if ($this->getIsLocked() === true) {
            RedirectResponse::create('Forbidden', Response::HTTP_FORBIDDEN)->send();
        }

        $recipePath = $this->recipesDirectory . $recipeSlug . '.md';

        $deleteResult = unlink($recipePath);

        if ($deleteResult !== false) {
            $this->session->getFlashBag()->add('alert', 'Successfully deleted ' . $this->convertSlugToString($recipeSlug));
        } else {
            $this->session->getFlashBag()->add('alert', 'Unable to delete file.');
        }

        RedirectResponse::create('/')->send();
    }

    private function getIsLocked()
    {
        return file_exists($this->lockPath);
    }

    private function toggleLock()
    {
        if ($this->getIsLocked() === true) {
            unlink($this->lockPath);
        } else {
            file_put_contents($this->lockPath, "1");
        }

        RedirectResponse::create('/')->send();
    }

    public function handleRequest()
    {
        $route = $this->request->getPathInfo();

        if ($route === '' || $route === '/') {
            $this->renderHomePage();
        } elseif (preg_match('/\/show\/([a-zA-Z0-9\-]*)$/', $route, $matches)) {
            $this->renderShowRecipe($matches[1]);
        } elseif (preg_match('/\/edit\/([a-zA-Z0-9\-]*)$/', $route, $matches)) {
            $this->editRecipe($matches[1]);
        } elseif (preg_match('/\/delete\/([a-zA-Z0-9\-]*)$/', $route, $matches)) {
            $this->deleteRecipe($matches[1]);
        } elseif ($route === '/new') {
            $this->newRecipe();
        } elseif ($route === '/lock') {
            $this->toggleLock();
        } elseif ($this->request->getMethod() === Request::METHOD_POST &&
            preg_match('/\/search\/(.*)$/', $route, $matches)) {
            $this->searchRecipes($matches[1]);
        } else {
            Response::create('Page not found', Response::HTTP_NOT_FOUND);
        }
    }
}

$request = Request::createFromGlobals();
$controller = new Controller($request);
$controller->handleRequest();
